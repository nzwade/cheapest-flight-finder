import datetime as dt

import geopy
import pandas as pd


def parse_ghcnd_stations():
    col_specs = [(0, 11), (12, 20), (21, 30), (31, 37), (38, 40), (41, 71), (72, 75), (76, 79), (80, 85)]
    ghcnd_stations = pd.read_fwf('ghcndfiles/ghcnd-stations.txt',
                                 colspecs=col_specs,
                                 header=None,
                                 index_col=None)
    ghcnd_stations.columns = ['id', 'latitude', 'longitude', 'elevation', 'state', 'name', 'gsn_flag', 'hcn_flag', 'wmoid']
    ghcnd_stations = ghcnd_stations.set_index('id')
    ghcnd_stations = ghcnd_stations.sort_values('name')

    ghcnd_stations['latitude'] = ghcnd_stations['latitude'].astype(str)
    ghcnd_stations['longitude'] = ghcnd_stations['longitude'].astype(str)
    ghcnd_stations['geopy_object'] = ghcnd_stations.apply(
        lambda x: geopy.Point.from_string(x['latitude'] + ' ' + x['longitude']), axis=1)
    return ghcnd_stations


def parse_ghcnd_inventory():
    col_specs = [(0, 11), (12, 20), (21, 30), (31, 35), (36, 40), (41, 45)]
    ghcnd_inventory = pd.read_fwf('ghcndfiles/ghcnd-inventory.txt',
                                  colspecs=col_specs,
                                  header=None,
                                  index_col=None)
    ghcnd_inventory.columns = ['id', 'latitude', 'longitude', 'element', 'first_year', 'last_year']
    ghcnd_inventory = ghcnd_inventory.set_index('id')

    ghcnd_inventory['latitude'] = ghcnd_inventory['latitude'].astype(str)
    ghcnd_inventory['longitude'] = ghcnd_inventory['longitude'].astype(str)
    ghcnd_inventory['geopy_object'] = ghcnd_inventory.apply(
        lambda x: geopy.Point.from_string(x['latitude'] + ' ' + x['longitude']), axis=1)
    return ghcnd_inventory


def parse_dly(weather_stations):
    colspecs = [(0, 11), (11, 15), (15, 17), (17, 21)]
    col_names = ['id', 'year', 'month', 'element']
    col_count = 21
    for day in range(31):
        v = [(col_count, col_count + 5), (col_count + 5, col_count + 6), (col_count + 6, col_count + 7),
             (col_count + 7, col_count + 8)]
        colspecs.extend(v)
        v = ['value%s' % (day + 1), 'mflag%s' % (day + 1), 'qflag%s' % (day + 1), 'sflag%s' % (day + 1)]
        col_names.extend(v)
        col_count += 8
    weather_df = pd.DataFrame()
    for weather_station in weather_stations:
        if weather_station == 'Vincenty formula failed to converge!':
            continue
        dly_df = pd.read_fwf('dlyfiles/{}.dly'.format(weather_station),
                             colspecs=colspecs,
                             names=col_names,
                             header=None)
        dly_df = dly_df[(dly_df['year'] > 2005) & (dly_df['year'] < 2017)]
        parsed_rows = []
        for index, row in dly_df.iterrows():
            for day in range(31):
                try:
                    date = dt.datetime(row['year'], row['month'], day + 1)
                except ValueError:
                    continue
                parsed_row = {'id': row['id'],
                              'date': date,
                              'year': row['year'],
                              'month': row['month'],
                              'day': day + 1,
                              'element': row['element'],
                              'value': row['value{}'.format(day + 1)],
                              'mflag': row['mflag{}'.format(day + 1)],
                              'qflag': row['qflag{}'.format(day + 1)],
                              'sflag': row['sflag{}'.format(day + 1)]}
                parsed_rows.append(parsed_row)
        dly_df = pd.DataFrame(parsed_rows)
        dly_df = dly_df[dly_df['value'] != -9999]  # Drop missing values
        dly_df = dly_df[dly_df['element'].isin(['prcp', 'tmin', 'tavg', 'tmax', 'snow', 'snwd'])]
        # Convert temperatures to degrees celsius and snow to CM
        dly_df.loc[dly_df['element'].isin(['prcp', 'tmin', 'tavg', 'tmax', 'snow', 'snwd']), 'value'] /= 10
        dly_df = dly_df.pivot(index='date', columns='element', values='value')
        dly_df = dly_df.reset_index()
        dly_df['id'] = weather_station
        dly_df['month'] = dly_df['date'].dt.month
        dly_df['day'] = dly_df['date'].dt.day
        dly_df = dly_df.drop('date', axis=1)
        dly_df = dly_df.groupby(['id', 'month', 'day']).mean()
        dly_df = dly_df.round(1)
        weather_df = weather_df.append(dly_df)
    return weather_df
