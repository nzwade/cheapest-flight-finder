import requests


def get_easyjet(inbounddate, outbounddate, origin, destination, adults, infants, children, teens):
    url = 'https://www.easyjet.com/ejavailability/api/v155/availability/query'
    params = {'AdditionalSeats': 0,
              'AdultSeats': adults,
              'ArrivalIata': destination,
              'ChildSeats': children,
              'DepartureIata': origin,
              'IncludeAdminFees': 'true',
              'IncludeFlexiFares': 'false',
              'IncludeLowestFareSeats': 'true',
              'IncludePrices': 'true',
              'Infants': infants,
              'IsTransfer': 'false',
              'LanguageCode': 'EN',
              'MaxDepartureDate': inbounddate,
              'MaxReturnDate': inbounddate,
              'MinDepartureDate': outbounddate,
              'MinReturnDate': outbounddate
              }
    headers = {'Cookie': 'ejCC_3=v=5690137702580922954&i=-8586819242677367992; cookies.js=1; _ga=GA1.3.1441924807.1519682659; lang2012=en-gb; idb*=%7B%22departure%22%3Anull%2C%22when%22%3Anull%2C%22flag%22%3Anull%2C%22price%22%3Anull%2C%22destination%22%3Anull%2C%22fromDate%22%3Anull%2C%22toDate%22%3Anull%2C%22customDate%22%3Anull%2C%22mapLocation%22%3Anull%2C%22moveMap%22%3Anull%2C%22region%22%3A%22en-gb%22%7D; odb*=LGW; __qca=P0-59682560-1519682664857; cookies_accepted=1; labi=81a8abec-b33f-f293-bd0c-26fa36775137; CMSPOD=fra-sc1-green; RBKPOD=dub-rbk-green; AVLPOD=dub-avl-green; lang=en; PHPSESSID=i6d3tliegu28hd6cvfh0ch93q1; _vwo_uuid_v2=DC82EB773076FE9C26678F2984F4B4B7B|3c359ae143f0d72f60a1733ae36d36ff; _ga=GA1.2.1441924807.1519682659; bd4ti=eZx1-oYhJUzy.1520721792204; _abck=912159D16C8252249AF6600374E65F5D5F6581C4BF2900004684945A087CC244~0~aulr1bTYpTXiSGEBgXtUE8uSOaKZksCzfBkpq3mgOaw=~-1~-1; bm_sz=446D3117C2141206BCCB56B809EE9C2E~QAAQnYFlX9ZaSUdiAQAA1WtISpIxDDoj5nellQ/Hdveghko4w0bo9+cKrJ7kGFSpkm7vxXQTWAeYN/16yOt7U9+uSc6SpNm0shcZ8RfMqfv3iw/0ikLH1RDBrxwp/SIJOMgBAHggTNrT/6hCDqs/DwV72DGHxLnpgrRlOAgNS1P1XTFvB5FWpvgVG4wrQI+D; _gid=GA1.3.2048913975.1521664685; bm_mi=2B40EFA6F9BB30CB972A0346F68D6CAC~q5dB9RsUN/DU+svfjsqGI2fHYHp1apgY0My3tIDfSRXaKBKnMiOlK0zSwDcOP75E1aIFO7i5DeVOrzjskA1ESn/BGr8GgpFn7Vag7gRGcj9fkhwaZWqdYUAw9kycV2QdTUdHyb0ETHtPnxuUhhmn4H8M9dmyLVnkeJgbeFem4RzY8ARViGhHoZxfhV2NWtvrZLEWdXjV2V0vLYOyR0W7ldqP3rARGbfDlI9RKgPurhHcc6JmmkZjzDZsyRjwbwpMqP9rIJvJtDWTS/WsNMQM6w==; ak_bmsc=28ED526EC45C87C5F735B91371D30F485F658195E2170000ACC2B25ADE54A229~plCRIJboFcYHKFWlKphamL8wK/O5LpNa5kSAwHcU7Xvml3KQlMOGncSXFPLx97fYmQngMeemEOkFQFwkfAaV/XDR+85Jn0VmnyHQA6oa9m0/RUUIlJ7Im3mbM3UVLNGMccFc8tPqApj0V66JqUzXXMErUo1/XjEIxDTCwBifcwYNxgv2oqeKznfr8GIxuxVxWBkQ9YH0Z2WVhp7atuhvJ/suLpv8fdMNQ7pe1CfbBFRALN9wo7HTcxGxNxei/NXMjx; 47620=; _uetsid=_uet637714af; WPPOD=1; ADRUM=s=1521666418848&r=https%3A%2F%2Fwww.easyjet.com%2Fen%2F%3F0; FunnelQuery=%7b%22OriginIata%22%3a%22LGW%22%2c%22DestinationIata%22%3a%22PRG%22%2c%22OutboundDate%22%3a%222018-04-20%22%2c%22ReturnDate%22%3a%222018-04-22%22%2c%22OutboundFlightNumber%22%3anull%2c%22ReturnFlightNumber%22%3anull%2c%22FunnelJourney%22%3a%22default%22%2c%22NumberOfAdults%22%3a2%2c%22NumberOfChildren%22%3a0%2c%22NumberOfInfants%22%3a0%2c%22OpenSearchPanel%22%3afalse%2c%22ShowFlexiFares%22%3afalse%2c%22RemainOnStep1%22%3afalse%2c%22ComponentSender%22%3a%22SearchPod2_%2fen%2f%22%2c%22CurrencyCode%22%3anull%2c%22PaymentTypeCode%22%3anull%7d; _gat=1; ej20SearchCookie=ej20Search_0=LGW|PRG|2018-04-20T00:00:00|2018-04-22T00:00:00|2|0|0|False||0|2018-03-21 21:07:05Z; ej20RecentSearches=ej20RecentSearch_0=LGW|PRG|2018-04-20T00:00:00|2018-04-22T00:00:00|2|0|0|False||0|2018-03-21 21:07:05Z&ej20RecentSearch_1=LGW|PRG|2018-03-09T00:00:00|2018-03-11T00:00:00|1|0|0|False||1|2018-02-26 22:31:29Z; FunnelJourney=%7B%22CorrelationToken%22%3A%227AED2B90-AC26-60A5-9BC0-58C0C88E21E1%22%2C%22PaymentType%22%3Anull%2C%22Origin%22%3A%22LGW%22%2C%22Destination%22%3A%22PRG%22%2C%22Outbound%22%3A%222018-04-20%22%2C%22Return%22%3A%222018-04-22%22%2C%22Component%22%3A%22SearchPod2_%2Fen%2F%22%2C%22SearchPanelOpen%22%3Afalse%2C%22FlexiFares%22%3Afalse%2C%22Adults%22%3A2%2C%22Children%22%3A0%2C%22Infants%22%3A0%2C%22JourneyPairId%22%3A1%2C%22CarSearchQuery%22%3Anull%2C%22Pairs%22%3A%5B%5D%2C%22FunnelJourney%22%3A%22default%22%7D; bm_sv=A5648FA5E83C5E628E73BDF9FE8D653E~4uQA1I/eXwXVYswGXYt/Tt7mWKZRAc6GrsMgW1fTcX8L4k0dBH2zPqILBkOT79zzlUzwIhFMscuPLH2mUhrEiXD2FUYGdOxMVYRd6L7T7P4J0AaaiSyybeUlCIrBWv+T5N6xJafN6iwbIA+fuj/luDbcy6B+c0ySr4F6PT/SpfM=; mmapi.store.p.0=%7B%22mmparams.d%22%3A%7B%7D%2C%22mmparams.p%22%3A%7B%22uat%22%3A%221553200689932%7C%7B%5C%22Domain%5C%22%3A%5C%22en%5C%22%2C%5C%22DepAirport%5C%22%3A%5C%22lgw%5C%22%2C%5C%22Destination%5C%22%3A%5C%22prg%5C%22%2C%5C%22TripDuration%5C%22%3A%5C%221to3%5C%22%2C%5C%22LeadTime%5C%22%3A%5C%2229to60%5C%22%2C%5C%22PassengerQty%5C%22%3A%5C%222%5C%22%2C%5C%22PassengerType%5C%22%3A%5C%22couple%5C%22%2C%5C%22SaturdayNight%5C%22%3A%5C%22true%5C%22%7D%22%2C%22pd%22%3A%221553202422801%7C%5C%221497873692%7CFwAAAApVAgD%2Bg0XM6g8AAREAAUKv2K2YBACUs6Cxb4%2FVSHMRf%2BNkfdVIAAAAAP%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FAAZEaXJlY3QBARABAAIAAQAAAAAARbIBAEWyAQBFsgEAAAABANXWAAAGAAAAAUU%3D%5C%22%22%2C%22srv%22%3A%221553202422807%7C%5C%22ldnvwcgeu11%5C%22%22%7D%7D; mmapi.store.s.0=%7B%22mmparams.d%22%3A%7B%7D%2C%22mmparams.p%22%3A%7B%7D%7D; akacd_TrueClarity_SC=1522271227~rv=24~id=69679c469ae9d50981a9ae23d2605510'}
    session = requests.Session()
    response = session.get(url, params=params, headers=headers)
    response.raise_for_status()
    requests_json = response.json()
    # pp.pprint(requests_json)
    return requests_json


def get_ryanair(inbound_date, outbound_date, origin, destination, adults, infants, children, teens):
    url = 'https://desktopapps.ryanair.com/v2/en-gb/availability'
    params = {'DateIn': inbound_date,
              'DateOut': outbound_date,
              'FlexDaysIn': 6,
              'FlexDaysOut': 6,
              'Origin': origin,
              'Destination': destination,
              'ADT': adults,
              'INF': infants,
              'CHD': children,
              'TEEN': teens,
              'RoundTrip': 'true',
              'exists': 'false',
              'ToUS': 'AGREED'}
    r = requests.get(url, params=params)
    r.raise_for_status()
    requests_json = r.json()
    # pp.pprint(requests_json)
    return requests_json

# def skyscannersearch():
#     url = 'http://partners.api.skyscanner.net/apiservices/pricing/v1.0'
#
#     headers = {
#         'Content-Type': 'application/x-www-form-urlencoded'
#     }
#
#     payload = {
#         'cabinClass': 'Economy',
#         'country': 'UK',
#         'currency': 'GBP',
#         'locale': 'en-GB',
#         'locationSchema': 'lata',
#         'originplace': 'EDI',
#         'destinationplace': 'LHR',
#         'outbounddate': '2017-06-12',
#         'inbounddate': '2017-06-14',
#         'adults': 1,
#         'children': 0,
#         'infants': 0,
#         'apikey': 'prtl6749387986743898559646983194'
#     }
#     r = requests.post(url, headers=headers, data=payload)
#     print (r.headers)
#     print(r.text)
#     r.raise_for_status()
#     return r
