import datetime as dt
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def parse_easyjet(rjson):
    date_time_format = '%Y-%m-%dT%H:%M:%S'

    fare_currency = rjson['DepartureBaseCurrencyCode']

    flight_dict_list = []
    for flight in rjson['AvailableFlights']:
        f = {'flight_key': flight['SegmentId'],
             'flight_number': flight['CarrierCode'] + ' ' + str(flight['FlightNumber']),
             'origin': flight['DepartureIata'],
             'destination': flight['ArrivalIata'],
             'departure_time_local': dt.datetime.strptime(flight['LocalDepartureTime'], date_time_format),
             'arrival_time_local': dt.datetime.strptime(flight['LocalArrivalTime'], date_time_format),
             'fare': float(flight['FlightFares'][0]['Prices']['Adult']['PriceWithDebitCard']),
             'fares_left': int(flight['FlightFares'][0]['LowestFareSeatsAvailable']),
             'currency': fare_currency,
             'duration': '',
             'carrier': 'easyJet'}
        if f['fare'] == 0:
            logger.debug('%s flight %s has no fare - Skipping', f['carrier'], f['flight_key'])
            continue
        flight_dict_list.append(f)
    return flight_dict_list


def parse_ryanair(rjson):
    date_time_format = '%Y-%m-%dT%H:%M:%S.%f'

    fare_currency = rjson['currency']
    fare_time_utc = dt.datetime.strptime(rjson['serverTimeUTC'][:-1], date_time_format)

    flight_dict_list = []
    for trip in rjson['trips']:
        for date in trip['dates']:
            for flight in date['flights']:
                if flight['faresLeft'] == 0:
                    continue
                f = {'flight_key': flight['flightKey'],
                     'flight_number': flight['flightNumber'],
                     'origin': trip['origin'],
                     # 'originname': trip['originName'],
                     'destination': trip['destination'],
                     # 'destinationname': trip['destinationName'],
                     'departure_time_local': dt.datetime.strptime(flight['time'][0][:-1], date_time_format),
                     'departure_time_utc': dt.datetime.strptime(flight['timeUTC'][0][:-1], date_time_format),
                     'arrival_time_local': dt.datetime.strptime(flight['timeUTC'][1][:-1], date_time_format),
                     'arrival_time_utc': dt.datetime.strptime(flight['time'][1][:-1], date_time_format),
                     'duration': flight['duration'],
                     'fare': float(flight['regularFare']['fares'][0]['publishedFare']),
                     'fares_left': int(flight['faresLeft']),
                     'currency': fare_currency,
                     'fare_time_utc': fare_time_utc,
                     'carrier': 'Ryanair'}
                flight_dict_list.append(f)
    return flight_dict_list