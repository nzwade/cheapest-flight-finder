from geopy.distance import vincenty


def find_closest_weather_station(row, ghcnd_stations):
    smallest_distance = -1
    closest_weather_station = None
    # print('Finding closest weather station to: {}'.format(row['geopy_object']))
    # print(row)
    for index, weather_station in ghcnd_stations.iterrows():
        try:
            distance = vincenty(row['geopy_object'], weather_station['geopy_object']).kilometers
        except ValueError:
            row['closest_weather_station'] = 'Vincenty formula failed to converge!'
            row['closest_weather_station_name'] = 'Vincenty formula failed to converge!'
            row['km_to_weather_station'] = 'Vincenty formula failed to converge!'
            return row
        if smallest_distance == -1:
            smallest_distance = distance
            closest_weather_station = index
        elif distance < smallest_distance:
            smallest_distance = distance
            closest_weather_station = index
    # print ('Closest weather station is:')
    # print ghcndstations.loc[closest_weather_station]['geopy_object']
    row['closest_weather_station'] = closest_weather_station
    row['closest_weather_station_name'] = ghcnd_stations.loc[closest_weather_station]['NAME']
    row['km_to_weather_station'] = smallest_distance
    return row