import datetime as dt
import logging
import os
import pprint

import geopy
import matplotlib.pyplot as plt
import pandas as pd
import requests

import GetFlights
import GetWeather
import ParseFlights
import ParseWeather
import WeatherUtility

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

pp = pprint.PrettyPrinter(indent=4)

plt.style.use('ggplot')

# TODO test if this works with non adult tickets
# TODO show all options instead of one if multiple flights have same price
# TODO Add easyJet airports to airport list. e.g. LJU
# TODO we should only call APIs for airports we know work with that carrier
#  airport info in javascript of: http://www.easyjet.com/en/routemap
# Weather station to country code mappings: ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/daily/ghcnd-countries.txt


# Command line arguments
origins = ['STN',
           # 'LTN',
           'LGW']
destinations = [
    'PRG',
    # 'GRX', 'SVQ','AGP',
    # 'FAO','JMK','JTR',
    # 'FAO',
    # 'CIA', 'FCO',
    # 'JMK', 'JTR',
    # 'ATH',
    # 'IBZ',
    # 'PMI',
    # 'MAH'
]
start_date = '2018-04-05'
end_date = '2018-05-02'
trip_lengths = [4]
max_days_off_work = 2
weekend = [5, 6]
adults = 2
infants = 0
children = 0
teens = 0
outbound_later_than = '00:00'
outbound_earlier_than = '17:00'
inbound_later_than = '08:00'
inbound_earlier_than = '19:00'
carriers = [
    'easyJet',
    'Ryanair',
]
outbound_previous_day = True
outbound_previous_day_later_than = '20:00'
outbound_previous_day_earlier_than = '23:59'

# Convert command line arguments to correct data type
start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
outbound_later_than = dt.datetime.strptime(outbound_later_than, '%H:%M').time()
outbound_earlier_than = dt.datetime.strptime(outbound_earlier_than, '%H:%M').time()
outbound_previous_day_later_than = dt.datetime.strptime(outbound_previous_day_later_than, '%H:%M').time()
outbound_previous_day_earlier_than = dt.datetime.strptime(outbound_previous_day_earlier_than, '%H:%M').time()
inbound_later_than = dt.datetime.strptime(inbound_later_than, '%H:%M').time()
inbound_earlier_than = dt.datetime.strptime(inbound_earlier_than, '%H:%M').time()

# Console messages
# TODO create formula to find minimum required days_off_work
#     raise ValueError('Trip length and/or max days off work and/or weekend days invalid.')
# days	min days_off_work
# 1	0
# 2	0
# 3	1
# 4	2
# 5	3
# 6	4
# 7	5
# 8	5
# 9	5
# 10	6
# 11	7
# 12	8
# 13	9
# 14	10
# 15	10
# 16	10
# 17	11
# 18	12
# 19	13
# 20	14
# 21	15
# 22	15
# 23	15
# 24	16
# 25	17

print(
    'Finding cheapest return trips from {} to {} anytime between {} and {} for {} days taking no more than {} days off work.'
        .format(origins, destinations, start_date.date(), end_date.date(), trip_lengths, max_days_off_work)
)
print(
    'Outbound flight must be between {} and {} or {} and {} the previous day (if the previous day is not in a weekend).'
        .format(outbound_later_than, outbound_earlier_than, outbound_previous_day_later_than,
                outbound_previous_day_earlier_than)
)
print('Inbound flight must be between {} and {}.'.format(inbound_later_than, inbound_earlier_than))
print('Prices are per ticket for {} adult(s).'.format(adults))

########################
# Get airports and weather
def get_ryanair_airports_json():
    url = 'https://desktopapps.ryanair.com/en-gb/res/stations'
    r = requests.get(url)
    r.raise_for_status()
    requestsjson = r.json()
    # pp.pprint(requestsjson)
    return requestsjson


def parse_ryanair_airports(ryanair_airports_json):
    ryanair_airports = pd.DataFrame(ryanair_airports_json)
    ryanair_airports = ryanair_airports.transpose()
    ryanair_airports = ryanair_airports.sort_values('name')

    ryanair_airports['latitude'] = ryanair_airports['latitude'].astype(str)
    ryanair_airports['longitude'] = ryanair_airports['longitude'].astype(str).str[1:]
    ryanair_airports['latitude_parsed'] = (ryanair_airports['latitude'].str[0:2]
                                          + ' '
                                          + ryanair_airports['latitude'].str[2:4]
                                          + 'm '
                                          + ryanair_airports['latitude'].str[4:6]
                                          + 's '
                                          + ryanair_airports['latitude'].str[6])

    ryanair_airports['longitude_parsed'] = (ryanair_airports['longitude'].str[0:2]
                                           + ' '
                                           + ryanair_airports['longitude'].str[2:4]
                                           + 'm '
                                           + ryanair_airports['longitude'].str[4:6]
                                           + 's '
                                           + ryanair_airports['longitude'].str[6])
    ryanair_airports['geopy_object'] = ryanair_airports.apply(
        lambda x: geopy.Point.from_string(x['latitude_parsed'] + ' ' + x['longitude_parsed']),
        axis=1
    )
    return ryanair_airports


if not os.path.isfile('airports.csv'):
    print('Downloading list of ryanair airports')
    ryanair_airports_json = get_ryanair_airports_json()
    print('DONE')
    print('Parsing list of ryanair airports')
    ryanair_airports = parse_ryanair_airports(ryanair_airports_json)
    # ryanair_airports = ryanair_airports.head(20)  # To reduce airport count for testing
    print('DONE - {} ryanair airports loaded.'.format(ryanair_airports.shape[0]))
    print(ryanair_airports.head())
    # zakinthos_ryanair_airport = ryanair_airports.loc['ZTH']
    # print zakinthos_ryanair_airport
    GetWeather.get_ghcn_files()
    print('Parsing list of weather stations.')
    ghcnd_inventory = ParseWeather.parse_ghcnd_inventory()
    active_weather_stations = ghcnd_inventory.loc[((ghcnd_inventory['FIRSTYEAR'] <= 2006)
                                                   & (ghcnd_inventory['LASTYEAR'] >= 2016)), 'ID']
    ghcnd_stations = ParseWeather.parse_ghcnd_stations()
    ghcnd_stations = ghcnd_stations[ghcnd_stations.index.isin(active_weather_stations)]
    print('DONE - {} weather stations loaded.'.format(ghcnd_stations.shape[0]))
    print(ghcnd_stations.head())
    # zakinthosghcnd = ghcndstations[ghcndstations['NAME'].str.contains('ZAKINTHOS')]
    # print zakinthosghcnd
    print ('Finding closest weather station to each airport.')
    airports = ryanair_airports.apply(WeatherUtility.find_closest_weather_station, axis=1, ghcndstations=ghcnd_stations)
    airports.to_csv('airports.csv', encoding='utf-8')
    print('DONE')
    print(airports.head())
if not os.path.isfile('weather.csv'):
    airports = pd.read_csv('airports.csv')
    # airports = airports.head(2) # for testing
    print('Downloading historical weather data for closest weather stations.')
    weather_stations = airports['closest_weather_station'].unique()
    GetWeather.get_dly_files(weather_stations)
    print('DONE')
    print('Parsing weather data and calculating historical averages.')
    weather = ParseWeather.parse_dly(weather_stations)
    weather.to_csv('weather.csv', encoding='utf-8')
    print('DONE')
########################

# Build list of all possible trip days and list of all dates
total_days = (end_date - start_date).days + 1
all_possible_trips = []
all_dates = [start_date + dt.timedelta(days=day) for day in range(total_days)]

for trip_length in trip_lengths:
    for date in all_dates:
        trip_days = [date + dt.timedelta(days=tripday) for tripday in range(trip_length)]
        number_of_weekend_days = 0
        for date in trip_days:
            if date.weekday() in weekend:
                number_of_weekend_days += 1
        days_off_work = int(trip_length - number_of_weekend_days)
        if days_off_work <= max_days_off_work:
            trip = {'outbound_date': trip_days[0],
                    'inbound_date': trip_days[-1],
                    'days_off_work': days_off_work,
                    'trip_length': trip_length}
            all_possible_trips.append(trip)
airports = pd.read_csv('airports.csv', index_col=0)

# Create dataframe with all flights
flights = pd.DataFrame()
for origin in origins:
    for destination in destinations:
        if 'easyJet' in carriers:
            print('Getting easyJet flights from {} {} to {} {}.'.format(airports.loc[origin, 'name'],
                                                                        airports.loc[origin, 'country'],
                                                                        airports.loc[destination, 'name'],
                                                                        airports.loc[destination, 'country']))
            # Max number of days returned by API is 3 so we have to call it multiple times
            for date in all_dates[::3]:
                outbound_date = dt.datetime.strftime(date, '%Y-%m-%d')
                date = date + dt.timedelta(days=2)
                inbound_date = dt.datetime.strftime(date, '%Y-%m-%d')
                easyjet_flights_json = GetFlights.get_easyjet(inbound_date, outbound_date, origin, destination,
                                                              adults, infants, children, teens)
                if easyjet_flights_json['AvailableFlights']:
                    parsed_easyjet_flights_json = ParseFlights.parse_easyjet(easyjet_flights_json)
                    flights = flights.append(pd.DataFrame(parsed_easyjet_flights_json))
        if 'Ryanair' in carriers:
            print('Getting Ryanair flights from {} {} to {} {}.'.format(airports.loc[origin, 'name'],
                                                                        airports.loc[origin, 'country'],
                                                                        airports.loc[destination, 'name'],
                                                                        airports.loc[
                                                                            destination, 'country']))

            # Max number of days returned by API is 7 so we have to call it multiple times
            for date in all_dates[::7]:
                outbound_date = dt.datetime.strftime(date, '%Y-%m-%d')
                inbound_date = dt.datetime.strftime(date, '%Y-%m-%d')
                ryanair_flights_json = GetFlights.get_ryanair(inbound_date, outbound_date, origin, destination,
                                                              adults, infants, children, teens)
                if ryanair_flights_json:
                    parsed_ryanair_flights_json = ParseFlights.parse_ryanair(ryanair_flights_json)
                    flights = flights.append(pd.DataFrame(parsed_ryanair_flights_json))

flights = flights.set_index('flight_key')
flights = flights.sort_index(axis=1)
# flights = flights[flights['fares_left'] != -1]
flights = flights.sort_values('departure_time_local')
flights.to_csv('flights.csv')


def find_minimum_fare(outbound_later_than, outbound_earlier_than, flights, trip, origin, destination, inboundlaterthan,
                      inbound_earlier_than, airports):
    outbound_flights = flights[(flights['departure_time_local'].dt.date == trip['outbound_date'].date())
                                 & (flights['departure_time_local'].dt.time >= outbound_later_than)
                                 & (flights['departure_time_local'].dt.time <= outbound_earlier_than)
                                 & (flights['origin'] == origin)]
    if outbound_flights.empty:
        return None
    outbound_minimum_fare_flight_key = outbound_flights['fare'].idxmin()
    outbound_flight = flights.loc[outbound_minimum_fare_flight_key]
    outbound_minimum_fare = outbound_flight['fare']

    outbound_origin_airport = airports.loc[outbound_flight['origin']]
    outbound_destination_airport = airports.loc[outbound_flight['destination']]

    inbound_flights = flights[(flights['departure_time_local'].dt.date == trip['inbound_date'].date())
                                & (flights['departure_time_local'].dt.time > inboundlaterthan)
                                & (flights['departure_time_local'].dt.time < inbound_earlier_than)
                                & (flights['origin'] == destination)]
    if inbound_flights.empty:
        return None
    inbound_minimum_fare_flight_key = inbound_flights['fare'].idxmin()
    inbound_flight = flights.loc[inbound_minimum_fare_flight_key]
    inbound_minimum_fare = inbound_flight['fare']

    inbound_origin_airport = airports.loc[inbound_flight['origin']]
    inbound_destination_airport = airports.loc[inbound_flight['destination']]

    t = {'outbound_date': outbound_flight['departure_time_local'].date(),
         # 'outbound_minimum_fareflightkey': outbound_minimum_fareflightkey,
         'outbound_departure_time_local': outbound_flight['departure_time_local'].time(),
         'outbound_arrival_time_local': outbound_flight['arrival_time_local'].time(),
         'outbound_duration': outbound_flight['duration'],
         'outbound_minimum_fare': outbound_minimum_fare,
         # 'outboundfaresleft': inbound_flight['faresleft'],
         'inbound_date': inbound_flight['departure_time_local'].date(),
         # 'inbound_minimum_fare_flight_key': inbound_minimum_fare_flight_key,
         'inbound_departure_time_local': inbound_flight['departure_time_local'].time(),
         'inbound_arrival_time_local': inbound_flight['arrival_time_local'].time(),
         'inbound_duration': inbound_flight['duration'],
         'inbound_minimum_fare': inbound_minimum_fare,
         # 'inboundfaresleft': outboundflight['faresleft'],
         'outbound_origin': outbound_origin_airport['name'],
         'outbound_destination': outbound_destination_airport['name'],
         'inbound_origin': inbound_origin_airport['name'],
         'inbound_destination': inbound_destination_airport['name'],
         'currency': inbound_flight['currency'],
         'minimum_total': outbound_minimum_fare + inbound_minimum_fare,
         'outbound_carrier': outbound_flight['carrier'],
         'inbound_carrier': inbound_flight['carrier'],
         'days_off_work': trip['days_off_work'],
         'trip_length': trip['trip_length'],
         'trip_name': outbound_origin_airport.name + outbound_destination_airport.name}
    return t

# Create DataFrame with all possible trips
trips = []
for origin in origins:
    for destination in destinations:
        for trip in all_possible_trips:
            t = find_minimum_fare(outbound_later_than, outbound_earlier_than, flights, trip, origin, destination,
                                  inbound_later_than, inbound_earlier_than, airports)
            if t is not None:
                trips.append(t)
            # Search for minimum fare departing on previous day
            if outbound_previous_day is False:
                continue
            previous_day_trip = trip.copy()
            previous_day_trip['outbound_date'] = previous_day_trip['outbound_date'] - dt.timedelta(days=1)
            # if previous_day_trip['outbound_date'].weekday() in weekend:
            #     continue
            t = find_minimum_fare(outbound_previous_day_later_than, outbound_previous_day_earlier_than, flights,
                                  previous_day_trip, origin, destination, inbound_later_than, inbound_earlier_than,
                                  airports)
            if t is not None:
                trips.append(t)

# Create CSV and PDF outputs
if trips:
    weather = pd.read_csv('weather.csv')
    weather = weather[['id', 'month', 'day', 'tavg']]  # Use these weather types only for now

    trip_weather = pd.DataFrame()
    for destination in destinations:
        weather_station = airports.loc[destination, 'closest_weather_station']
        weather_station_weather_closest = weather[weather['id'] == weather_station]
        weather_station_weather = pd.DataFrame()
        weather_station_weather['date'] = all_dates
        weather_station_weather['month'] = weather_station_weather['date'].dt.month
        weather_station_weather['day'] = weather_station_weather['date'].dt.day
        weather_station_weather['id'] = weather_station
        weather_station_weather = pd.merge(left=weather_station_weather,
                                           right=weather_station_weather_closest,
                                           on=['id', 'month', 'day'],
                                           how='left')
        weather_station_weather = weather_station_weather.drop(['id', 'month', 'day'], axis=1)
        weather_station_weather = weather_station_weather.dropna(axis=1)
        weather_station_weather = weather_station_weather.set_index('date')
        weather_station_weather.columns = ['{}_{}'.format(destination, col) for col in weather_station_weather.columns]
        trip_weather = pd.concat([trip_weather, weather_station_weather], axis=1)
    # print tripweather.head()

    trips = pd.DataFrame(trips)
    trips = trips.sort_index(axis=1)
    trips = trips.sort_values('minimum_total', ascending=True)
    trips.to_csv('trips.csv')
    # print(tripdf.head())

    trips_for_pdf = pd.pivot_table(data=trips,
                                     values='minimum_total',
                                     index='outbound_date',
                                     columns='trip_name',
                                     aggfunc=min)
    trips_for_pdf.columns.name = None
    trip_weather = trip_weather.reindex(index=trips_for_pdf.index)
    # print tripdfforpdf.head()

    plt.figure(figsize=(9, 7))
    ax1 = plt.subplot(211)
    trips_for_pdf.plot.line(ax=ax1)
    ax1.tick_params(labelbottom='off')
    ax1.set_xlabel('')
    ax1.set_ylabel('Minimum price per return ticket')
    ax2 = plt.subplot(212)
    trip_weather.plot.line(ax=ax2)
    ax2.set_xlabel('Outbound date')
    ax2.set_ylabel('Degrees celsius')

    plt.savefig('minimum_total.pdf')
    plt.show()

else:
    print('NO RESULTS')
