import os
from ftplib import FTP


def get_ghcn_files():
    if os.path.isfile('ghcndfiles/ghcnd-stations.txt') & os.path.isfile('ghcndfiles/ghcnd-inventory.txt'):
        return
    os.chdir('dlyfiles')
    ftp = FTP('ftp.ncdc.noaa.gov')
    ftp.set_debuglevel(1)
    ftp.login()
    ftp.cwd('pub/data/ghcn/daily/')
    if not os.path.isfile('ghcnd-stations.txt'):
        print('Downloading ghcnd-stations.txt')
        ftp.retrbinary('RETR ' + 'ghcnd-stations.txt', open('ghcnd-stations.txt', 'wb').write)
        print('DONE')
    if not os.path.isfile('ghcnd-inventory.txt'):
        print('Downloading ghcnd-inventory.txt')
        ftp.retrbinary('RETR ' + 'ghcnd-inventory.txt', open('ghcnd-inventory.txt', 'wb').write)
        print('DONE')
    ftp.quit()
    os.chdir('..')


def get_dly_files(weather_stations):
    ftp = FTP('ftp.ncdc.noaa.gov')
    ftp.set_debuglevel(1)
    ftp.login()
    ftp.cwd('pub/data/ghcn/daily/all/')
    os.chdir('dlyfiles')
    for weather_station in weather_stations:
        if weather_station == 'Vincenty formula failed to converge!':
            continue
        if not os.path.isfile('{}.dly'.format(weather_station)):
            ftp.retrbinary('RETR {}.dly'.format(weather_station), open('{}.dly'.format(weather_station), 'wb').write)
    os.chdir('..')
    ftp.quit()
